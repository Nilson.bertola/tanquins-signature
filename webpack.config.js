require('dotenv').config();
const HtmlWebPackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');
const Dotenv = require('dotenv-webpack');

const dotenv = new Dotenv({
  systemvars: true
});

const cpwp = new CopyPlugin([
  { from: './public/robots.txt', to: './' },
  { from: './public/sitemap.xml', to: './' }
]);

const html = new HtmlWebPackPlugin({
  template: './public/index.html',
  filename: './index.html',
  favicon: './public/favicon.ico'
});

const optimization = {
  production: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          warnings: false,
          compress: {
            comparisons: false
          },
          parse: {},
          mangle: true,
          output: {
            comments: false,
            ascii_only: true
          }
        },
        parallel: true,
        cache: true,
        sourceMap: true
      })
    ],
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true,
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 10,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          chunks: 'all',
          priority: 1
        }
      }
    }
  },
  development: {
    minimize: false
  }
};

const devServer = {
  development: {
    contentBase: path.join(__dirname, './dist'),
    port: 9000,
    historyApiFallback: true,
    publicPath: '/',
    open: true,
    hot: true
  },
  production: {}
};

const { NODE_ENV } = process.env;

module.exports = {
  devtool: 'source-map',
  mode: 'none',
  entry: {
    app: ['./src/index.js']
  },
  output: {
    path: path.join(__dirname, './dist'),
    publicPath: '/',
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.s?css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|jpg|gif|woff2|ico)$/,
        loader: 'file-loader'
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader'
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      }
    ]
  },
  plugins: [html, cpwp, dotenv],
  optimization: optimization[NODE_ENV],
  devServer: devServer[NODE_ENV]
};
