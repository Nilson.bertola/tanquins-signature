import React from 'react';
import { Transition } from 'react-transition-group';
import { InputStyles } from './styles';

const { InputElement, InputHolder, InputLabel, transLabel } = InputStyles;

const Input = ({ label, change, value, name, size = 'col-12' }) => (
  <InputHolder className={size}>
    <Transition in={value === ''} timeout={0}>
      {state => <InputLabel style={transLabel[state]}>{label}</InputLabel>}
    </Transition>
    <InputElement name={name} value={value} onChange={change} />
  </InputHolder>
);

export { Input };

