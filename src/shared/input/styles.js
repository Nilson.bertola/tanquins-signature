import styled from 'styled-components';
import { whiteDark } from '../../theme/colors';
import { mqXs } from '../../theme/mediaQueries';

const InputHolder = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 50px;
  margin: 2px 5px;
  ${mqXs} {
    width: 100%;
  }
`;

const InputLabel = styled.span`
  transition: all 0.4 ease-in-out;
`;

const transLabel = {
  entered: {
    fontSize: '18px',
    zIndex: 1
  },
  exited: {}
};

const InputElement = styled.input`
  border: none;
  border-radius: 0px;
  background-color: ${whiteDark}66;
  font-size: 16px;
  border-top-left-radius: 6px;
  border-top-right-radius: 6px;
`;

const InputStyles = { InputHolder, InputLabel, InputElement, transLabel };

export { InputStyles };

