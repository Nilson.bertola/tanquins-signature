export const fontIndieFlower = `"Indie Flower", cursive`;
export const fontAmatic = `'Amatic SC', cursive`;
export const fontVollkorn = `'Vollkorn SC', serif`;
export const fontOpenSans = `'Open Sans Condensed', sans-serif`;
export const fontCaveat = `'Caveat', cursive`;
export const fontGreatVibes = `'Great Vibes', cursive`;
export const fontPoiret = `'Poiret One', cursive`;
