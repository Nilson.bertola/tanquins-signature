export const blue = '#71CBD3';
export const blueLight = '#B1E2E7';
export const blueDark = '#3E6F74';
export const yellow = '#F8EA8C';
export const yellowLight = '#FBF3C0';
export const yellowDark = '#9E955A';
export const pink = '#F652A0';
export const pinkLight = '#F990C2';
export const pinkDark = '#872D58';
export const white = '#F8F5E5';
export const whiteLight = '#FBF9F0';
export const whiteDark = '#88867D';
export const gray = '#A2A392';
export const grayLight = '#D4D5CD';
export const grayDark = '#595950';
export const purple = '#8B1D96'

//

/**
 * https://coolors.co/a4e8e0-f8ea8c-f652a0-f8f5e5-a2a392
 */
