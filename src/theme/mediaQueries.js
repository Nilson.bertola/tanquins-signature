export const mqXs = "@media (max-width: 767px)";
export const mqSm = "@media (min-width: 768px)";
export const mqMd = "@media (min-width: 992px)";
export const mqLg = "@media (min-width: 1200px)";
