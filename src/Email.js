import React from 'react';

export const Email = ({ ga: { clickGA } }) => {
  return (
    <a href="mailto: contato@tanquins.com" onClick={() => clickGA('CONTATO')}>
      contato@tanquins.com
    </a>
  );
};
