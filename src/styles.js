import styled, { css } from 'styled-components';
import { blueLight, grayDark, purple, whiteDark, yellow, yellowLight } from './theme/colors';
import { fontPoiret } from './theme/fonts';

export const IndexMain = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  flex-direction: column;
  height: 100vh;
  background-color: ${blueLight};
`;

export const Signature = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: auto;
  height: 300px;
  padding: 15px;
  margin: 15px;
`;

export const Logo = styled.div`
  width: 250px;
  display: flex;
  flex-direction: row;
  align-content: center;
  justify-content: center;
  img {
    width: inherit;
    z-index: 2;
  }
`;

export const Rect = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  background-color: #fefefe;
  min-width: fit-content;
  max-width: fit-content;
  height: 200px;
  left: -120px;
  border-radius: 8px;
  box-shadow: 0px 0px 7px ${grayDark};
  padding-left: 80px;
  padding-right: 10px;
  ${({ props }) =>
    props.phone2 &&
    css`
  padding-top: 10px;
  padding-bottom: 10px;`}
`;

const Span = styled.span`
  margin-left: 50px;
  font-family: ${fontPoiret} !important;
`;

export const InfoName = styled(Span)`
  font-size: 17px;
  font-weight: bold;
  letter-spacing: 1px;
  text-transform: capitalize;
`;

export const InfoPosition = styled(Span)`
  font-size: 16px;
  letter-spacing: 1px;
  text-transform: capitalize;
`;

export const Details = styled(Span)`
  letter-spacing: 0.5px;
  i,
  img,
  svg {
    fill: ${purple};
    font-size: 24px;
    height: 24px;
    width: 24px;
    position: relative;
    top: 5px;
    color: ${purple};
    margin: 3px;
  }
`;

export const Form = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const BtnToPng = styled.div`
  cursor: pointer;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 3px 7px;
  background-color: ${yellowLight};
  color: ${grayDark};
  border-radius: 6px;
  transition: all 0.6s;
  :hover {
    background-color: ${yellow};
    color: ${whiteDark};
  }
  i,
  span {
    margin: 4px 8px;
  }
`;
