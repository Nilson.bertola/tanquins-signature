import * as htmlToImage from 'html-to-image';
import React, { useEffect, useRef } from 'react';
import { BtnClose, ImageHolder, ModalHolder, ModalMain } from './styles';

export const Modal = ({ turnOff, htmlRef }) => {
  const holder = useRef(null);
  useEffect(() => {
    htmlToImage
      .toPng(htmlRef.current)
      .then(function(dataUrl) {
        var img = new Image();
        img.src = dataUrl;
        holder.current.appendChild(img);
      })
      .catch(function(error) {
        console.error('oops, something went wrong!', error);
      });
  }, []);
  return (
    <ModalMain>
      <ModalHolder>
        <ImageHolder ref={holder} />
        <span>Clicar com o Botão direito do mouse e clicar em Salvar Como...</span>
        <BtnClose onClick={() => turnOff(false)}>
          <i className="material-icons">close</i>
          <span>Fechar</span>
        </BtnClose>
      </ModalHolder>
    </ModalMain>
  );
};
