import styled from 'styled-components';
import { grayDark, whiteDark, whiteLight, yellow, yellowLight } from '../theme/colors';

export const ModalMain = styled.div`
  position: absolute;
  left: 0px;
  top: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 100vh;
  width: 100vw;
  background-color: ${grayDark}33;
  z-index: 10;
`;

export const ModalHolder = styled.div`
  background-color: ${whiteLight};
  border-radius: 10px;
`;

export const ImageHolder = styled.div`
`;

export const BtnClose = styled.div`
  cursor: pointer;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 3px 7px;
  background-color: ${yellowLight};
  color: ${grayDark};
  border-radius: 6px;
  transition: all 0.6s;
  :hover {
    background-color: ${yellow};
    color: ${whiteDark};
  }
  i,
  span {
    margin: 4px 8px;
  }
`;
