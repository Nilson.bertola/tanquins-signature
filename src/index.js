import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import logo from './assets/images/logo_small.png';
import { Modal } from './modal';
import { Input } from './shared/input';
import { BtnToPng, Details, Form, IndexMain, InfoName, InfoPosition, Logo, Rect, Signature } from './styles';
import './styles/fonts.css';
import './styles/grid.css';
import './styles/reset.css';
import { purple } from './theme/colors';

const Index = () => {
  const [values, setValues] = useState({});
  const [modal, setModal] = useState(false);
  const sig = useRef(null);

  const handleInput = ({ target: { name, value } }) => {
    setValues({ ...values, [name]: value });
  };
  return (
    <IndexMain>
      <Form>
        <Input
          name="name"
          value={values.name || ''}
          change={handleInput}
          label="Nome"
          size="col-3"
        />
        <Input
          name="position"
          value={values.position || ''}
          change={handleInput}
          label="Posição"
          size="col-3"
        />
        <Input
          name="phone"
          value={values.phone || ''}
          change={handleInput}
          label="Telefone"
          size="col-3"
        />
        <Input
          name="phone2"
          value={values.phone2 || ''}
          change={handleInput}
          label="Telefone Alternativo"
          size="col-3"
        />
        <Input
          name="email"
          value={values.email || ''}
          change={handleInput}
          label="Email"
          size="col-3"
        />
      </Form>
      <Signature ref={sig}>
        <Logo>
          <img src={logo} />
        </Logo>
        <Rect props={values}>
          <InfoName>{values.name || "Atendimento Tanquin's"}</InfoName>
          <InfoPosition>{values.position || 'Apoio'}</InfoPosition>
          <Details>&nbsp;</Details>
          {values.phone && (
            <Details>
              <i className="material-icons">sentiment_satisfied_alt</i>
              {values.phone}
            </Details>
          )}
          {values.phone2 && (
            <Details>
              <i className="material-icons">sentiment_satisfied_alt</i>
              {values.phone2}
            </Details>
          )}

          <Details>
            <i className="material-icons">important_devices</i>
            www.tanquins.com
          </Details>

          <Details>
            <i className="material-icons">mail_outline</i>
            {values.email || 'contato@tanquins.com'}
          </Details>

          <Details>
            <FontAwesomeIcon icon={faInstagram} style={{ color: purple }} />
            @tanquins_eventos
          </Details>
        </Rect>
      </Signature>
      <BtnToPng onClick={() => setModal(true)}>
        <i className="material-icons">compare</i>
        <span>Gerar Assinatura</span>
      </BtnToPng>
      {modal && <Modal turnOff={setModal} htmlRef={sig} />}
    </IndexMain>
  );
};

ReactDOM.render(<Index />, document.querySelector('#root'));
